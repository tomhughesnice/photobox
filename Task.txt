Please write all the source code necessary to implement the Story below. You should not spend more than a couple of hours on the exercise.
Please send us back your work no later than one working day before your technical interview where you will demonstrate your work.

Story:

As a taxpayer
I want to know how much taxes I am going to pay on my income
So that I can understand my contribution to this country

Acceptance criteria

Given that I am given a personal allowance of £10,600
And the personal allowance is deducted from my income
Then I want to know my taxable income

Given that my taxable income is below or equal £31,785
Then it will be tax at 20%

Given that my taxable income is below or equal £150,000
Then the amount of my taxable income below £31,785 will be tax at 20%
And the amount above will be tax at 40%

Given that my taxable income is above £150,000
Then the amount of my taxable income below £31,785 will be tax at 20%
And the amount between £31,786 and £150,000 will be tax at 40%
And the amount above £150,000 will be tax at 45%

