# Photobox interview test #

### This repository is intended to solve the following task ###

Story:

As a taxpayer
I want to know how much taxes I am going to pay on my income
So that I can understand my contribution to this country
 
Acceptance criteria

Given that I am given a personal allowance of £10,600
And the personal allowance is deducted from my income
Then I want to know my taxable income        

Given that my taxable income is below or equal £31,785
Then it will be tax at 20%

Given that my taxable income is below or equal £150,000
Then the amount of my taxable income below £31,785 will be tax at 20%
And the amount above will be tax at 40%
 
Given that my taxable income is above £150,000
Then the amount of my taxable income below £31,785 will be tax at 20%
And the amount between £31,786 and £150,000 will be tax at 40%
And the amount above £150,000 will be tax at 45%

### Run the test ###

This application is built using Maven, and uses Spring at Runtime. In order to run this application you need to have Maven(>3.0.0) and Java 8 installed.

* Checkout this repository to you local machine.
* Run the command 'mvn package' to run tests and build the application.
* Run the command 'mvn spring-boot:run' to start a local web service running on your local machine at port 8080.

Now the server is running a simple RESTful interface exists to calculate your income tax.

Navigate to the following url http://127.0.0.1:8080/Tax/{Income} , replace the curly braces with your income. For example http://127.0.0.1:8080/Tax/70000


### Extra ###
A secondary part of this test was todo the following.

Please build a Magnolia component displaying a user’s Username, email address and postal address. You should consider the attached pipe delimited text file as your data source.
Please send us back your work no later than one working day before your technical interview where you will demonstrate your work.
#### I have Implemented to the Loading of user data from the flat file, but have not used Magnolia before ####
