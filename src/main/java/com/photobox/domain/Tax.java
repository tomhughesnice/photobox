package com.photobox.domain;

/**
 * Created by thomashughes on 05/05/15.
 */
public class Tax {

    private float taxableIncome;

    private float lowerBracketTax;

    private float middleBracketTax;

    private float higherBracketTax;

    public Tax() {}

    public float getLowerBracketTax() {
        return lowerBracketTax;
    }

    public void setLowerBracketTax(float lowerBracketTax) {
        this.lowerBracketTax = lowerBracketTax;
    }

    public float getMiddleBracketTax() {
        return middleBracketTax;
    }

    public void setMiddleBracketTax(float middleBracketTax) {
        this.middleBracketTax = middleBracketTax;
    }

    public float getHigherBracketTax() {
        return higherBracketTax;
    }

    public void setHigherBracketTax(float higherBracketTax) {
        this.higherBracketTax = higherBracketTax;
    }

    public float getTotalTax() {
        return lowerBracketTax + middleBracketTax + higherBracketTax;
    }

    public float getTaxableIncome() {
        return taxableIncome;
    }

    public void setTaxableIncome(float taxableIncome) {
        this.taxableIncome = taxableIncome;
    }

    @Override
    public String toString() {
        return "Tax{" +
                "lowerBracketTax=" + lowerBracketTax +
                ", middleBracketTax=" + middleBracketTax +
                ", higherBracketTax=" + higherBracketTax +
                ", totalTax=" + getTotalTax() +
                '}';
    }
}
