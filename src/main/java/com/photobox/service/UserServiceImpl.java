package com.photobox.service;

import com.photobox.domain.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by thomashughes on 05/05/15.
 */
@Service
public class UserServiceImpl implements UserService {

    private final Log log = LogFactory.getLog(getClass());

    private final String splitRegex = "[|]";

    //Expected number of headers in flat file
    private int numberOfHeaders = 5;

    private File data;

    public UserServiceImpl() {
        ClassLoader classLoader = getClass().getClassLoader();
        data = new File(classLoader.getResource("data.txt").getFile());
    }

    /**
     * Calls a flat file and parses to a list of User objects.
     * @return A list of all available users
     */
    public List<User> loadAll() throws IllegalArgumentException {
        List<User> users = new ArrayList<User>();

        try (Scanner scanner = new Scanner(data)) {
            boolean firstLine = true;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                log.debug(line);
                //The first line should contain the headers
                if (firstLine) {
                    String[] headers = line.split(splitRegex);
                    if (headers.length != numberOfHeaders) {
                        throw new IllegalArgumentException("Headers size invalid");
                    }
                    firstLine = false;
                } else {
                    String[] data = line.split(splitRegex);
                    if (data.length != numberOfHeaders) {
                        throw new IllegalArgumentException("Data does not match headers");
                    }
                    User user = new User();
                    user.setName(data[0]);
                    user.setEmail(data[1]);
                    user.setAge(Integer.valueOf(data[2]));
                    user.setAddress(data[3]);
                    user.setUsername(data[4]);
                    users.add(user);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;

    }

}
