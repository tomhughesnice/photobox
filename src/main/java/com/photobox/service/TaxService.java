package com.photobox.service;

import com.photobox.domain.Tax;

/**
 * Created by thomashughes on 05/05/15.
 */
public interface TaxService {

    // Tax related constants
    public final int PERSONAL_ALLOWANCE = 10600;
    public final int TAX_RATE_MIDDLE_BOUNDS = 31785;
    public final int TAX_RATE_HIGH_BOUNDS = 150000;

    public final float TAX_RATE_LOWER_PERCENT = 0.20f;
    public final float TAX_RATE_MIDDLE_PERCENT = 0.40f;
    public final float TAX_RATE_HIGH_PERCENT = 0.45f;

    public Tax calculateTaxForIncome(float income);
}
