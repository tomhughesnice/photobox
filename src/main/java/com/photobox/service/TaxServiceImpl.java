package com.photobox.service;

import com.photobox.domain.Tax;
import org.springframework.stereotype.Service;

/**
 * A service to deal with all Tax calculations.
 * Created by thomashughes on 04/05/15.
 */
@Service
public class TaxServiceImpl implements TaxService {

    /**
     * Will work out the tax required for the income provided.
     * @param income
     * @return Tax An object containing the tax required against the income.
     */
    public Tax calculateTaxForIncome(float income) {
        Tax tax = new Tax();

        //The personal allowance is the bracket where no tax is required.
        float taxableIncome = income - PERSONAL_ALLOWANCE;

        // If income is less than or equal to person allowance no tax required
        if (taxableIncome > 0) {
            tax.setTaxableIncome(taxableIncome);

            //Work out how much income lies in the highest bracket
            if (tax.getTaxableIncome() > TAX_RATE_HIGH_BOUNDS) {
                tax.setHigherBracketTax((tax.getTaxableIncome() - TAX_RATE_HIGH_BOUNDS) * TAX_RATE_HIGH_PERCENT);
            }

            //Work out tax in the middle bracket
            if (tax.getTaxableIncome() > TAX_RATE_MIDDLE_BOUNDS) {
                tax.setMiddleBracketTax(((tax.getTaxableIncome() > TAX_RATE_HIGH_BOUNDS ? TAX_RATE_HIGH_BOUNDS : tax.getTaxableIncome())
                        - TAX_RATE_MIDDLE_BOUNDS) * TAX_RATE_MIDDLE_PERCENT);
            }

            //Work out my tax in the lowest bracket
            tax.setLowerBracketTax((tax.getTaxableIncome() > TAX_RATE_MIDDLE_BOUNDS ? TAX_RATE_MIDDLE_BOUNDS : tax.getTaxableIncome())
                    * TAX_RATE_LOWER_PERCENT);
        }
        return tax;
    }

}
