package com.photobox.service;

import com.photobox.domain.User;

import java.util.List;

/**
 * Created by thomashughes on 05/05/15.
 */
public interface UserService {

    public List<User> loadAll() throws IllegalArgumentException;
}
