package com.photobox.config;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;

/**
 * Simple Spring Boot configuration
 */
@EnableAutoConfiguration
@ComponentScan({"com.photobox.controller", "com.photobox.service"})
public class Config {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Config.class, args);
    }

}