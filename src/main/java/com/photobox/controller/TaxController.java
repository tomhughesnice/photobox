package com.photobox.controller;

import com.photobox.domain.Tax;
import com.photobox.service.TaxService;
import com.photobox.service.TaxServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by thomashughes on 05/05/15.
 */
@RestController
public class TaxController {

    @Autowired
    private TaxService taxService;

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/Tax/{income}")
    public Tax tax(@PathVariable float income) {
        Tax tax = taxService.calculateTaxForIncome(income);
        return tax;
    }

}
