package com.photobox;

import com.photobox.domain.User;
import com.photobox.service.UserServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

/**
 * Tests loading users from a flat file.
 * Created by thomashughes on 05/05/15.
 */
public class UserServiceTest {

    private final Log log = LogFactory.getLog(getClass());

    UserServiceImpl userDetailsService = new UserServiceImpl();

    /**
     * Tests loading all users from flat file.
     */
    @Test
    public void testLoadUsers() {

        List<User> users = userDetailsService.loadAll();
        assertThat(users).isNotEmpty();

        // Loop through all users for debug purposes
        for (User user : users) {
            log.debug(user);
        }

    }

}
