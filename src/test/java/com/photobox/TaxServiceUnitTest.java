package com.photobox;

import com.photobox.service.TaxService;
import com.photobox.service.TaxServiceImpl;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

/**
 *
 * Created by thomashughes on 05/05/15.
 */
public class TaxServiceUnitTest {

    private TaxService taxService = new TaxServiceImpl();

    @Test
    public void testHigherBracketTax() {
        float income = 170000;
        float expectedTax = 4230;
        assertThat(taxService.calculateTaxForIncome(income).getHigherBracketTax()).isEqualTo(expectedTax);

        income = 180000;
        expectedTax = 8730;
        assertThat(taxService.calculateTaxForIncome(income).getHigherBracketTax()).isEqualTo(expectedTax);

        income = 100000;
        expectedTax = 0;
        assertThat(taxService.calculateTaxForIncome(income).getHigherBracketTax()).isEqualTo(expectedTax);
    }

    @Test
    public void testMiddleBracketTax() {
        float income = 160000;
        float expectedTax = 47046;
        assertThat(taxService.calculateTaxForIncome(income).getMiddleBracketTax()).isEqualTo(expectedTax);

        income = 100000;
        expectedTax = 23046;
        assertThat(taxService.calculateTaxForIncome(income).getMiddleBracketTax()).isEqualTo(expectedTax);
    }

    @Test
    public void testLowerBracketTax() {
        float income = 160000;
        float expectedTax = 6357;
        assertThat(taxService.calculateTaxForIncome(income).getLowerBracketTax()).isEqualTo(expectedTax);

        income = 9000;
        expectedTax = 0;
        assertThat(taxService.calculateTaxForIncome(income).getLowerBracketTax()).isEqualTo(expectedTax);

        income = 10610;
        expectedTax = 2;
        assertThat(taxService.calculateTaxForIncome(income).getLowerBracketTax()).isEqualTo(expectedTax);
    }



}
