package com.photobox;

import com.photobox.config.Config;
import com.photobox.controller.TaxController;
import com.photobox.domain.Tax;
import com.photobox.service.TaxService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.*;

/**
 * Integration tests for the following story
 *
 * As a taxpayer
 * I want to know how much taxes I am going to pay on my income
 * So that I can understand my contribution to this country
 *
 * Created by thomashughes on 05/05/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
@IntegrationTest
public class TaxControllerTest {

    @Autowired
    private TaxController taxController;

    /**
     * Given that I am given a personal allowance of £10,600
     * And the personal allowance is deducted from my income
     * Then I want to know my taxable income
     */
    @Test
    public void testShowMyTaxableIncome() {
        float income = 50000;
        Tax tax = taxController.tax(income);
        assertThat(tax.getTaxableIncome()).isNotZero();
        assertThat(tax.getTaxableIncome()).isGreaterThan(0f);
    }

    /**
     * Given that my taxable income is below or equal £31,785
     * Then it will be tax at 20%
     */
    @Test
    public void testLowerBoundsIncome() {
        float income = 31785 + TaxService.PERSONAL_ALLOWANCE;
        Tax tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(6357);

        income = 16000 + TaxService.PERSONAL_ALLOWANCE;
        tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(3200);
    }

    /**
     * Given that my taxable income is below or equal £150,000
     * Then the amount of my taxable income below £31,785 will be tax at 20%
     * And the amount above will be tax at 40%
     */
    @Test
    public void testMiddleBoundsIncome() {
        float income = 150000 + TaxService.PERSONAL_ALLOWANCE;
        Tax tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(53643);

        income = 75000 + TaxService.PERSONAL_ALLOWANCE;
        tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(23643);
    }

    /**
     * Given that my taxable income is above £150,000
     * Then the amount of my taxable income below £31,785 will be tax at 20%
     * And the amount between £31,786 and £150,000 will be tax at 40%
     * And the amount above £150,000 will be tax at 45%
     */
    @Test
    public void testHigherBoundsIncome() {
        float income = 500000 + TaxService.PERSONAL_ALLOWANCE;
        Tax tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(211143);

        income = 160000 + TaxService.PERSONAL_ALLOWANCE;
        tax = taxController.tax(income);
        assertThat(tax.getTotalTax()).isNotZero();
        assertThat(tax.getTotalTax()).isEqualTo(58143);
    }


}
